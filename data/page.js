var PageItemsData = [
    { name: 'chum', imgSrc: './pics/chum.png', text: '伙伴蛋' },
    { name: 'balloon', imgSrc: './pics/icon_balloon.webp', text: '气球/地图制图师' },
    { name: 'station', imgSrc: './pics/icon_station.webp', text: '传送点/营地' },
    { name: 'temple', imgSrc: './pics/icon_temple.webp', text: '神庙/解谜点' },
    { name: 'quest', imgSrc: './pics/quest.webp', text: '任务相关' },
];

PageItemsData.forEach(function(item,index) {
    let newItem = document.createElement('div');
    newItem.id = 'item-' + index;  
    newItem.className = 'items';
    newItem.onclick = function() {
        toggleMarkers(item.name, index);
    };

    let img = document.createElement('img');
    img.src = item.imgSrc;
    let textNode = document.createTextNode(item.text);
    let checker = document.createElement('div');
    checker.className = 'checker';
    checker.id = item.name + 'Checker';  

    newItem.appendChild(img);
    newItem.appendChild(textNode);
    newItem.appendChild(checker);

    document.getElementById('itemsContainer').appendChild(newItem);
});

function toggleSidePanel() {
    let sidePanel = document.getElementById('sidePanel');
    let sideToggle = document.getElementById('sideToggle');
    let sidePanelWidth = sidePanel.offsetWidth;  
    if (sidePanel.classList.contains('panelHidden')) {
        sidePanel.classList.remove('panelHidden');
        sideToggle.classList.remove('left');
        sideToggle.classList.add('right');
        sideToggle.style.transform = 'translateX(0)'; 
    } else {
        sidePanel.classList.add('panelHidden');
        sideToggle.classList.remove('right');
        sideToggle.classList.add('left');
        sideToggle.style.transform = 'translateX(' + (sidePanelWidth) + 'px)';  
    }
}

window.onload = function() {
    let sideToggle = document.getElementById('sideToggle');
    sideToggle.addEventListener('click', toggleSidePanel);
}

function openImage(src) {
    let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if (isMobile) {
        window.open(src, '_blank');
    } else {
        document.getElementById('overlayImage').src = src;
        document.getElementById('overlay').style.display = 'flex';
    
    }
}

document.getElementById('overlay').addEventListener('click', function(i) {
    if (i.target.id === 'overlayImage') {
        return;
    }
    document.getElementById('overlay').style.display = 'none';
});

console.log('clientWidth:',document.documentElement.clientWidth);