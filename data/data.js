var itemData = {
	"items": [{
		"category": "chum",
		"id": 2000,
		"name": "伙伴蛋000",
		"Y": 323.625,
		"X": 591.125,
		"icon": "",
		"DESC": "<p>第一个激活磁石任务要去的遗迹门口附近</p><img src='https://i0.hdslb.com/bfs/article/764a184cf86e8a1030893d070f4fab153405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2001,
		"name": "伙伴蛋001",
		"Y": 299.5,
		"X": 542.375,
		"icon": "",
		"DESC": "<p>村子外圆环附近的高台上，可以靠旁边的破船跳上去。</p><img src='https://i0.hdslb.com/bfs/article/3b2d9c37813d42de0680d701f856a8683405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2002,
		"name": "伙伴蛋002",
		"Y": 284.5,
		"X": 554.25,
		"icon": "",
		"DESC": "<p>初始村伊别克斯营地内</p><img src='https://i0.hdslb.com/bfs/article/865d8a6d88a45e156cf4ffe3a05e398a3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2003,
		"name": "伙伴蛋003",
		"Y": 333.924,
		"X": 594.375,
		"icon": "",
		"DESC": "<p>一开始激活磁石的神庙的高柱子上，后期升级了体力回来拿比较好。</p><img src='https://i0.hdslb.com/bfs/article/9c3d40d32da43d643708115a3d06a88b3405001.png@1e_1c.webp'><img src='https://i0.hdslb.com/bfs/article/6e00ccd6721aab0a7448c8243df6f0fd3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2004,
		"name": "伙伴蛋004",
		"Y": 314.329,
		"X": 503.925,
		"icon": "",
		"DESC": "<p>沙海地图中间的塔楼上面</p><img src='https://i0.hdslb.com/bfs/article/2dd6d4dea404c84303dfa7b6fe397cb53405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2005,
		"name": "伙伴蛋005",
		"Y": 273.64,
		"X": 573.225,
		"icon": "",
		"DESC": "<p>初始地图，东北，路过一个有甲虫的区域后往里走。（抓这里的甲虫有奖杯）</p><img src='https://i0.hdslb.com/bfs/article/422f36966241ecacd81ea01831f965f93405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2006,
		"name": "伙伴蛋006",
		"Y": 273.115,
		"X": 576.45,
		"icon": "",
		"DESC": "<p>初始地图，东北高台，一个有甲虫的区域上方建筑残骸上</p><img src='https://i0.hdslb.com/bfs/article/981da711df92009738b5f7a98d36a83c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2007,
		"name": "伙伴蛋007",
		"Y": 285.487,
		"X": 580.95,
		"icon": "",
		"DESC": "<p>初始地图，东侧高台上</p><img src='https://i0.hdslb.com/bfs/article/5015bd32f64e579c1b3482db9732262e3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2008,
		"name": "伙伴蛋008",
		"Y": 278.989,
		"X": 594.352,
		"icon": "",
		"DESC": "<p>初始地图，寻找原子控制面板的飞船附近，初始体力可以拿到，但是要从区域入口大门开关那里从山上爬过去。</p><img src='https://i0.hdslb.com/bfs/article/2306e190bd38d5f03a5a5c103a2ae4823405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2009,
		"name": "伙伴蛋009",
		"Y": 292.86,
		"X": 592.7,
		"icon": "",
		"DESC": "<p>初始地图，寻找原子控制面板的飞船附近，从木船残骸可以爬上去</p><img src='https://i0.hdslb.com/bfs/article/b2d4645dfaf683817afe29a7e2f6dcad3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2010,
		"name": "伙伴蛋010",
		"Y": 320.603,
		"X": 574.375,
		"icon": "",
		"DESC": "<p>如图所示的小山峰上面</p><img src='https://i0.hdslb.com/bfs/article/f5bd63350f6be02c241bbbcd9960f8863405001.png@1e_1c.webp'><img src='https://i0.hdslb.com/bfs/article/ea8d6aca86694387580bd14ac0619b1f3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2011,
		"name": "伙伴蛋011",
		"Y": 328.475808,
		"X": 572.250015,
		"icon": "",
		"DESC": "<p>011号蛋附近的倒扣木船下面</p><img src='https://i0.hdslb.com/bfs/article/e16e552ddee045e289e341bd38f956a63405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2012,
		"name": "伙伴蛋012",
		"Y": 335.8488,
		"X": 571.25,
		"icon": "",
		"DESC": "<p>取得原子功能面板的飞船旁边跳下去</p><img src='https://i0.hdslb.com/bfs/article/029488477676cc44960ab74d1717878b3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2013,
		"name": "伙伴蛋013",
		"Y": 297.302,
		"X": 356.325,
		"icon": "",
		"DESC": "<p>女王巢穴附近</p><img src='https://i0.hdslb.com/bfs/article/f836d4273a63c56d5f796c96e9bb7eba3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2014,
		"name": "伙伴蛋014",
		"Y": 294.802,
		"X": 352.575,
		"icon": "",
		"DESC": "<p>女王巢穴附近</p><img src='https://i0.hdslb.com/bfs/article/fbb08b852604e87cd2664ccfbd9ebcba3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2015,
		"name": "伙伴蛋015",
		"Y": 292.928,
		"X": 370.45,
		"icon": "",
		"DESC": "<p>女王巢穴附近的石门遗迹上方（升级过一次体力后能爬上去或从高处飘过去）</p><img src='https://i0.hdslb.com/bfs/article/896f2a3a409ea80a43956e7061935f093405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2016,
		"name": "伙伴蛋016",
		"Y": 301,
		"X": 467.95,
		"icon": "",
		"DESC": "<p>沙海地图中部的台地上方</p><img src='https://i0.hdslb.com/bfs/article/e4423a7fb39bf1bf1528e4c7467403293405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2017,
		"name": "伙伴蛋017",
		"Y": 291.255,
		"X": 470.45,
		"icon": "",
		"DESC": "<p>沙海地图中部的台地上方</p><img src='https://i0.hdslb.com/bfs/article/60543d9d37f460ca279f0e0dd679a7bd3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2018,
		"name": "伙伴蛋018",
		"Y": 293.62958,
		"X": 469.75,
		"icon": "",
		"DESC": "<p>沙海地图中部的台地中空空间里面的塔上面</p><img src='https://i0.hdslb.com/bfs/article/dc7ded0c60f3b2dafcbedfde54e503f93405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2019,
		"name": "伙伴蛋019",
		"Y": 297.128,
		"X": 467.2,
		"icon": "",
		"DESC": "<p>沙海地图中部的台地中空空间里面的沙堆上面</p><img src='https://i0.hdslb.com/bfs/article/b1d52b303b81a8b7804ebc4d688471b23405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2020,
		"name": "伙伴蛋020",
		"Y": 205.48237,
		"X": 413.25,
		"icon": "",
		"DESC": "<p>焦橡村子旁边</p><img src='https://i0.hdslb.com/bfs/article/d696438f151f73c3d715aa221fd05aec3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2021,
		"name": "伙伴蛋021",
		"Y": 210.5,
		"X": 413.75,
		"icon": "",
		"DESC": "<p>焦橡村子里面旅店上方的弯曲柱子顶部</p><img src='https://i0.hdslb.com/bfs/article/938dfe98da586edb367c835d67e7adde3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2022,
		"name": "伙伴蛋022",
		"Y": 199.9838,
		"X": 413.375,
		"icon": "",
		"DESC": "<p>焦橡村子旁边的石柱上</p><img src='https://i0.hdslb.com/bfs/article/99cd3d6d76b6234fd1b00db1b36dfc523405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2023,
		"name": "伙伴蛋023",
		"Y": 178.264658,
		"X": 478.1,
		"icon": "",
		"DESC": "<p>邓博因号旁边的红色塔顶部</p><img src='https://i0.hdslb.com/bfs/article/0bfa2d98a8b4b2b5f3afa03f6054d9dd3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2024,
		"name": "伙伴蛋024",
		"Y": 183.013386,
		"X": 467.475,
		"icon": "",
		"DESC": "<p>邓博因号北侧的管子里面</p><img src='https://i0.hdslb.com/bfs/article/d5fd8ec0707c58f0cd4a848c2ee275ea3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2025,
		"name": "伙伴蛋025",
		"Y": 188.5119,
		"X": 462.85,
		"icon": "",
		"DESC": "<p>邓博因号尾部喷管附近</p><img src='https://i0.hdslb.com/bfs/article/10cf9c9fcff8cc0a4b4c99dd6557736f3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2026,
		"name": "伙伴蛋026",
		"Y": 187.512181,
		"X": 470.85,
		"icon": "",
		"DESC": "<p>邓博因号内部，进入人工智能房间之前的大房间</p><img src='https://i0.hdslb.com/bfs/article/46b695b8321994e85889db75594f271e3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2027,
		"name": "伙伴蛋027",
		"Y": 197.8844,
		"X": 473.35,
		"icon": "",
		"DESC": "<p>邓博因号南侧有枯树的小高台上面</p><img src='https://i0.hdslb.com/bfs/article/f54538cb50ee72118d312605518cbab23405001.png@1e_1c.webp'><p></p><img src='>"
	},
	{
		"category": "chum",
		"id": 2028,
		"name": "伙伴蛋028",
		"Y": 375.521415,
		"X": 576.85,
		"icon": "",
		"DESC": "<p>风塔下面</p><img src='https://i0.hdslb.com/bfs/article/0c80e9800a84b84a0df9d2001e8b7e393405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2029,
		"name": "伙伴蛋029",
		"Y": 373.89685,
		"X": 589.6,
		"icon": "",
		"DESC": "<p>风塔东侧台地顶峰上面</p><img src='https://i0.hdslb.com/bfs/article/fbc771f95309a315f1026140461844ab3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2030,
		"name": "伙伴蛋030",
		"Y": 382.89444,
		"X": 575.35,
		"icon": "",
		"DESC": "<p>风塔西侧靠下的位置</p><img src='https://i0.hdslb.com/bfs/article/e9cf000e3638eec18d752355e4cdd2053405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2031,
		"name": "伙伴蛋031",
		"Y": 383.75,
		"X": 584.125,
		"icon": "",
		"DESC": "<p>风塔南侧峭壁的木板上</p><img src='https://i0.hdslb.com/bfs/article/c643573f8a6e02564151365c2f1838c53405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2032,
		"name": "伙伴蛋032",
		"Y": 376.1109,
		"X": 582.625,
		"icon": "",
		"DESC": "<p>风塔链接桥附近</p><img src='https://i0.hdslb.com/bfs/article/aeed8046a8da2e8722b15944230ce5a43405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2033,
		"name": "伙伴蛋033",
		"Y": 386.747489,
		"X": 459.75,
		"icon": "",
		"DESC": "<p>赫拉克勒斯甲虫窝东北侧的两个柱子的台地上面</p><img src='https://i0.hdslb.com/bfs/article/89567ac95b42f4c054d4fa1570d4eff33405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2034,
		"name": "伙伴蛋034",
		"Y": 397.369644,
		"X": 445.95,
		"icon": "",
		"DESC": "<p>赫拉克勒斯甲虫窝，取得果实的建筑下方的房间里面。</p><img src='https://i0.hdslb.com/bfs/article/aab9af24c25775d76531c45ec96f14183405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2035,
		"name": "伙伴蛋035",
		"Y": 404.2428,
		"X": 437.2,
		"icon": "",
		"DESC": "<p>赫拉克勒斯甲虫窝，甲虫巢穴，从顶部爬出来，往下看</p><img src='https://i0.hdslb.com/bfs/article/f2bdda749dba676ba8f5f33fe0490a8e3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2036,
		"name": "伙伴蛋036",
		"Y": 408.241733,
		"X": 440.325,
		"icon": "",
		"DESC": "<p>赫拉克勒斯甲虫窝，甲虫巢穴，外部的管子可以进去，里面有一只蛋和染料</p><img src='https://i0.hdslb.com/bfs/article/bd98c56783d26d0acd4eebe98d696dc33405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2037,
		"name": "伙伴蛋037",
		"Y": 406.988557,
		"X": 433.625,
		"icon": "",
		"DESC": "<p>赫拉克勒斯甲虫窝，甲虫巢穴内部，途中有藤蔓的地方可以爬进去</p><img src='https://i0.hdslb.com/bfs/article/cf9d89cdbe7bc6f0380058c0b4f228d13405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2038,
		"name": "伙伴蛋038",
		"Y": 141.714221,
		"X": 268.725,
		"icon": "",
		"DESC": "<p>森林中的石台上</p><img src='https://i0.hdslb.com/bfs/article/f717215fd14e2fbad9310f66e1cbad2d3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2039,
		"name": "伙伴蛋039",
		"Y": 156.460273,
		"X": 277.85,
		"icon": "",
		"DESC": "<p>森林中的石台上</p><img src='https://i0.hdslb.com/bfs/article/b221cdc77522f3f0b83d792eec188a473405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2040,
		"name": "伙伴蛋040",
		"Y": 172.45599,
		"X": 287.225,
		"icon": "",
		"DESC": "<p>森林中的石台上的高柱子上（体力升级2次之后来）</p><img src='https://i0.hdslb.com/bfs/article/0630d3f8c55b5bde3173bdafb7a5cda83405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2041,
		"name": "伙伴蛋041",
		"Y": 162.341,
		"X": 221.975,
		"icon": "",
		"DESC": "<p>地图绘制员区域，下面有巨大面具的地方附近</p><img src='https://i0.hdslb.com/bfs/article/735677aabfba1fa983d93ab420e19bf63405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2042,
		"name": "伙伴蛋042",
		"Y": 167.7159,
		"X": 222.725,
		"icon": "",
		"DESC": "<p>降落伞附近的高台上，可以从地图绘制员那里飘过去</p><img src='https://i0.hdslb.com/bfs/article/375ef47d6b8e1eb5c68098f9d7d660ec3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2043,
		"name": "伙伴蛋043",
		"Y": 161.967,
		"X": 226.225,
		"icon": "",
		"DESC": "<p>降落伞附近的屋顶上，可以从地图绘制员那里飘过去</p><img src='https://i0.hdslb.com/bfs/article/16d6d6b02664dae8bf8f83cf635334913405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2044,
		"name": "伙伴蛋044",
		"Y": 162.467369,
		"X": 218.6,
		"icon": "",
		"DESC": "<p>地图绘制员身后的屋顶上</p><img src='https://i0.hdslb.com/bfs/article/6b059304db04dade0d8a55f4baa721353405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2045,
		"name": "伙伴蛋045",
		"Y": 227.2333,
		"X": 247.5,
		"icon": "",
		"DESC": "<p>拜尔弗隆联道号入口附近</p><img src='https://i0.hdslb.com/bfs/article/986d82201cdc63df6876ffc89c058eb03405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2046,
		"name": "伙伴蛋046",
		"Y": 230.98,
		"X": 243.375,
		"icon": "",
		"DESC": "<p>拜尔弗隆联道号内部</p><img src='https://i0.hdslb.com/bfs/article/4892edc32b3d44d6b4d96bfbd7af09493405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2047,
		"name": "伙伴蛋047",
		"Y": 226.2333,
		"X": 243.125,
		"icon": "",
		"DESC": "<p>拜尔弗隆联道号内部</p><img src='https://i0.hdslb.com/bfs/article/eb34b5175e5a639ee37700e185ca6ab33405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2048,
		"name": "伙伴蛋048",
		"Y": 229.4827,
		"X": 248.625,
		"icon": "",
		"DESC": "<p>拜尔弗隆联道号入口上方的机翼上</p><img src='https://i0.hdslb.com/bfs/article/904c6623024d62a78a421e3b087255403405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2049,
		"name": "伙伴蛋049",
		"Y": 156,
		"X": 127,
		"icon": "",
		"DESC": "<p>艾克利亚的房屋顶上</p><img src='https://i0.hdslb.com/bfs/article/6da73fc4f4d0ac56f48c7d721eefb4263405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2050,
		"name": "伙伴蛋050",
		"Y": 165.354,
		"X": 124.5,
		"icon": "",
		"DESC": "<p>艾克利亚入口的柱子顶上</p><img src='https://i0.hdslb.com/bfs/article/dbfbd91bee3f5e2ab181534c060159663405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2051,
		"name": "伙伴蛋051",
		"Y": 158.731,
		"X": 129.625,
		"icon": "",
		"DESC": "<p>艾克利亚东侧的房屋顶上</p><img src='https://i0.hdslb.com/bfs/article/7de162523091e6075a9ad338d411a1413405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2052,
		"name": "伙伴蛋052",
		"Y": 152.857,
		"X": 126.875,
		"icon": "",
		"DESC": "<p>艾克利亚北侧，从湖泊进入下水道</p><img src='https://i0.hdslb.com/bfs/article/12de1fd64f03b5ba1a6feb3ab15dcee03405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2053,
		"name": "伙伴蛋053",
		"Y": 145.99,
		"X": 114.875,
		"icon": "",
		"DESC": "<p>艾克利亚湖泊西侧岸边的石台上</p><img src='https://i0.hdslb.com/bfs/article/85effe7aa153e75bf94f9b23d4d27f2b3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2054,
		"name": "伙伴蛋054",
		"Y": 144.99,
		"X": 120.375,
		"icon": "",
		"DESC": "<p>艾克利亚湖泊里面靠西的石台上</p><img src='https://i0.hdslb.com/bfs/article/0919de739958db13e198a6e0f06f5bb63405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2055,
		"name": "伙伴蛋055",
		"Y": 142.366468,
		"X": 125.125,
		"icon": "",
		"DESC": "<p>艾克利亚湖泊中部靠北的小岛上</p><img src='https://i0.hdslb.com/bfs/article/317ca2362e7261dac7eda95c64de4ba43405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2056,
		"name": "伙伴蛋056",
		"Y": 144.865798,
		"X": 130.875,
		"icon": "",
		"DESC": "<p>艾克利亚湖泊东侧岸边的石柱上</p><img src='https://i0.hdslb.com/bfs/article/f9702760a80971853db417e3dd8fcbf83405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2057,
		"name": "伙伴蛋057",
		"Y": 97.12858,
		"X": 127.125,
		"icon": "",
		"DESC": "<p>赤沙中部沙漠的一段管子上</p><img src='https://i0.hdslb.com/bfs/article/815e01f2dc5fc57e48a07aec135095773405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2058,
		"name": "伙伴蛋058",
		"Y": 55.4397,
		"X": 103.4,
		"icon": "",
		"DESC": "<p>原子之心内部（需要在“城中心碎”任务中拿到门禁卡才能进入）</p><img src='https://i0.hdslb.com/bfs/article/967a228f086f2d717a3a1594282978683405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2059,
		"name": "伙伴蛋059",
		"Y": 59.063771,
		"X": 106.275,
		"icon": "",
		"DESC": "<p>原子之心内部核心房间（需要在“城中心碎”任务中拿到门禁卡才能进入）</p><img src='https://i0.hdslb.com/bfs/article/ed7b95c4981e937a511cfb3a3dc33a743405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2060,
		"name": "伙伴蛋060",
		"Y": 57.189273,
		"X": 110.775,
		"icon": "",
		"DESC": "<p>原子之心外面的管子上</p><img src='https://i0.hdslb.com/bfs/article/45f20bb0af17ef5efffbeab39ed6e8703405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2061,
		"name": "伙伴蛋061",
		"Y": 135.75097,
		"X": 76.95,
		"icon": "",
		"DESC": "<p>艾克利亚西边不远处的五铃营地的大钟下面</p><img src='https://i0.hdslb.com/bfs/article/759ed9e334c60afb6531a81d2be5d2ce3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2062,
		"name": "伙伴蛋062",
		"Y": 431.496487,
		"X": 394.57499,
		"icon": "",
		"DESC": "<p>恶地神秘的神庙，屋顶上</p><img src='https://i0.hdslb.com/bfs/article/8235fd0bb5d1d82fd2748df1e0f890903405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2063,
		"name": "伙伴蛋063",
		"Y": 432.1213,
		"X": 390.044,
		"icon": "",
		"DESC": "<p>恶地神秘神庙上方山顶上，需要从另一个方向爬上去</p><img src='https://i0.hdslb.com/bfs/article/69134f0517a7867199f3e2e62afadf5b3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2064,
		"name": "伙伴蛋064",
		"Y": 470.735,
		"X": 374.425,
		"icon": "",
		"DESC": "<p>肯博的立方飞船外部的管子里面</p><img src='https://i0.hdslb.com/bfs/article/dec1eb1e11de6fe510d7b7437bb353c03405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2065,
		"name": "伙伴蛋065",
		"Y": 465.137471,
		"X": 378.8,
		"icon": "",
		"DESC": "<p>肯博的立方飞船内部的管子上</p><img src='https://i0.hdslb.com/bfs/article/6e8e05cc5615a8d43cbc90f594154df93405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2066,
		"name": "伙伴蛋066",
		"Y": 467.7617,
		"X": 378.675,
		"icon": "",
		"DESC": "<p>肯博的立方飞船顶部见到攀登者的平台</p><img src='https://i0.hdslb.com/bfs/article/45f20bb0af17ef5efffbeab39ed6e8703405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2067,
		"name": "伙伴蛋067",
		"Y": 468.13666,
		"X": 375.175,
		"icon": "",
		"DESC": "<p>肯博的立方飞船外部突出部分</p><img src='https://i0.hdslb.com/bfs/article/22e6b701c097ac6b2e6c441f132a581f3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2068,
		"name": "伙伴蛋068",
		"Y": 479.758556,
		"X": 379.3,
		"icon": "",
		"DESC": "<p>肯博的立方飞船南侧小山上的管子里，体力两3圈半以上才能爬上去，体力不够可以从飞船顶部飘过去</p><img src='https://i0.hdslb.com/bfs/article/f4b9263c992849e717596d60348226ed3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2069,
		"name": "伙伴蛋069",
		"Y": 521.222,
		"X": 376.3,
		"icon": "",
		"DESC": "<p>劳利斯路船头附近</p><img src='https://i0.hdslb.com/bfs/article/acd535b3f622f26c411b74dcd790ba523405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2070,
		"name": "伙伴蛋070",
		"Y": 518.848,
		"X": 371.42499,
		"icon": "",
		"DESC": "<p>劳利斯路船头北侧的管子里</p><img src='https://i0.hdslb.com/bfs/article/9546dbdf677c214d58483e53875821e43405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2071,
		"name": "伙伴蛋071",
		"Y": 530.595,
		"X": 366.425,
		"icon": "",
		"DESC": "<p>劳利斯路尾部喷管里</p><img src='https://i0.hdslb.com/bfs/article/8a76fe0fae6855b0acc599e7e7c832df3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2072,
		"name": "伙伴蛋072",
		"Y": 526.97,
		"X": 371.174,
		"icon": "",
		"DESC": "<p>劳利斯路船身下方</p><img src='https://i0.hdslb.com/bfs/article/4509b51d65bbce5103f60cf77c822bb73405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2073,
		"name": "伙伴蛋073",
		"Y": 476.6587,
		"X": 341.9,
		"icon": "",
		"DESC": "<p>圆环塔顶部</p><img src='https://i0.hdslb.com/bfs/article/9f103ed6d3b09d191a737bed930f0fb33405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2074,
		"name": "伙伴蛋074",
		"Y": 618.437,
		"X": 268.925,
		"icon": "",
		"DESC": "<p>冷光虫洞入口瀑布的石柱顶部</p><img src='https://i0.hdslb.com/bfs/article/516c8d4fe4d959d246d6ae05025ada3a3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2075,
		"name": "伙伴蛋075",
		"Y": 621.311,
		"X": 273.675,
		"icon": "",
		"DESC": "<p>冷光虫洞入口瀑布左侧下方</p><img src='https://i0.hdslb.com/bfs/article/b3004ab95658b86805b1389541c67cd73405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2076,
		"name": "伙伴蛋076",
		"Y": 627.184916,
		"X": 262.8,
		"icon": "",
		"DESC": "<p>冷光虫洞内部</p><img src='https://i0.hdslb.com/bfs/article/dcb065b6739a078a790b795a7c2385c53405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2077,
		"name": "伙伴蛋077",
		"Y": 391.7977,
		"X": 315.25,
		"icon": "",
		"DESC": "<p>恶地地图绘制员附近</p><img src='https://i0.hdslb.com/bfs/article/bf271222a0ec5a4379def530fb4ce9523405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2078,
		"name": "伙伴蛋078",
		"Y": 434.16137,
		"X": 321.9,
		"icon": "",
		"DESC": "<p>山顶村子的屋顶上（旁边商人可以买巢雕机车配件）</p><img src='https://i0.hdslb.com/bfs/article/5e24b2ccb972559f0eba3ea937d5aea13405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2079,
		"name": "伙伴蛋079",
		"Y": 434.4113,
		"X": 335.025,
		"icon": "",
		"DESC": "<p>山顶村子的建筑顶部（下面宝箱有一件衣服）</p><img src='https://i0.hdslb.com/bfs/article/fd09d36a0f3c19dda74fae1c53e2ac2d3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2080,
		"name": "伙伴蛋080",
		"Y": 371.775,
		"X": 220.675,
		"icon": "",
		"DESC": "<p>叛桥断桥西南部分，靠东侧的峭壁平台上</p><img src='https://i0.hdslb.com/bfs/article/51895468116677bb6895443c83f21c2c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2081,
		"name": "伙伴蛋081",
		"Y": 373.4,
		"X": 211.3,
		"icon": "",
		"DESC": "<p>叛桥断桥西南部分，塔楼顶部，要从旁边绿色塔顶飘过去</p><img src='https://i0.hdslb.com/bfs/article/06517b5fee7a2c65c8c80ea026887f033405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2082,
		"name": "伙伴蛋082",
		"Y": 368.4,
		"X": 213.3,
		"icon": "",
		"DESC": "<p>叛桥断桥西南部分，绿色塔顶建筑下面</p><img src='https://i0.hdslb.com/bfs/article/a51e7b1f6db4933fad67cd7933cdd1933405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2083,
		"name": "伙伴蛋083",
		"Y": 372.899,
		"X": 216.05,
		"icon": "",
		"DESC": "<p>叛桥断桥西南部分，房屋顶部</p><img src='https://i0.hdslb.com/bfs/article/81872a56a9f6baba18eedac56774c9b33405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2084,
		"name": "伙伴蛋084",
		"Y": 363.027,
		"X": 229.675,
		"icon": "",
		"DESC": "<p>叛桥断桥东北部分，雕像旁边的墙边</p><img src='https://i0.hdslb.com/bfs/article/f935725011cbbb8dfc3e5af1369353273405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2085,
		"name": "伙伴蛋085",
		"Y": 358.403,
		"X": 230.549,
		"icon": "",
		"DESC": "<p>叛桥断桥东北部分，雕像后面的塔顶</p><img src='https://i0.hdslb.com/bfs/article/444d2c57d43c8afa2e0abba91a5c4c8f3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2086,
		"name": "伙伴蛋086",
		"Y": 182.54,
		"X": 93.65,
		"icon": "",
		"DESC": "<p>贼喜鹊井进口的起重机上面，要从旁边的高处飘过去</p><img src='https://i0.hdslb.com/bfs/article/059deec65c067a7e716081e31bb994093405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2087,
		"name": "伙伴蛋087",
		"Y": 183.914784,
		"X": 92.525,
		"icon": "",
		"DESC": "<p>贼喜鹊井往下第一层走廊</p><img src='https://i0.hdslb.com/bfs/article/d12934838b9c94cebb54657a69e325f33405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2088,
		"name": "伙伴蛋088",
		"Y": 181.040554,
		"X": 89.775,
		"icon": "",
		"DESC": "<p>接近井底的位置</p><img src='https://i0.hdslb.com/bfs/article/6dade3d15f7f61b8ae9ac105860e7b673405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2089,
		"name": "伙伴蛋089",
		"Y": 187.28888,
		"X": 86.65,
		"icon": "",
		"DESC": "<p>井口附近的石头上</p><img src='https://i0.hdslb.com/bfs/article/1e39c900bbc44b3586ae75a0ff28fc513405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2090,
		"name": "伙伴蛋090",
		"Y": 190.31308,
		"X": 94.2,
		"icon": "",
		"DESC": "<p>井口东南侧的狭窄地点,可以从井边看到蔓藤，钻进去</p><img src='https://i0.hdslb.com/bfs/article/ffb3056ecc9306ba911b2ec1f8a0ed383405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2091,
		"name": "伙伴蛋091",
		"Y": 215.2064,
		"X": 131.85,
		"icon": "",
		"DESC": "<p>圆环塔顶部</p><img src='https://i0.hdslb.com/bfs/article/92e31e233dc77e496d8159340434efcd3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2092,
		"name": "伙伴蛋092",
		"Y": 519.36,
		"X": 551.75,
		"icon": "",
		"DESC": "<p>髓骨站停车位旁边</p><img src='https://i0.hdslb.com/bfs/article/5c92c13667d8a0e008e300471d358b203405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2093,
		"name": "伙伴蛋093",
		"Y": 520.110445,
		"X": 548.625,
		"icon": "",
		"DESC": "<p>093号蛋后面不远的草堆里面</p><img src='https://i0.hdslb.com/bfs/article/c88439b454dff06f1cfcba98e3201afa3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2094,
		"name": "伙伴蛋094",
		"Y": 518.735,
		"X": 555.75,
		"icon": "",
		"DESC": "<p>髓骨站上面骨架的最高处</p><img src='https://i0.hdslb.com/bfs/article/835f58ed44f6fc143ce02445108b1c013405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2095,
		"name": "伙伴蛋095",
		"Y": 508.86446,
		"X": 623.75,
		"icon": "",
		"DESC": "<p>绘图师南部下面的平台的管子里</p><img src='https://i0.hdslb.com/bfs/article/d958e60a036ed61fc7355a18c34ca1933405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2096,
		"name": "伙伴蛋096",
		"Y": 501.491434,
		"X": 623.25,
		"icon": "",
		"DESC": "<p>绘图师旁边飞船的喷管里</p><img src='https://i0.hdslb.com/bfs/article/02788cd2fc82679190bc545d6c2371863405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2097,
		"name": "伙伴蛋097",
		"Y": 499.866,
		"X": 624.5,
		"icon": "",
		"DESC": "<p>绘图师旁边飞船下方的小平台上面，要从上面飘下去</p><img src='https://i0.hdslb.com/bfs/article/2b9f38a580c114af1f802849ac0071463405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2098,
		"name": "伙伴蛋098",
		"Y": 498.367271,
		"X": 625.875,
		"icon": "",
		"DESC": "<p>绘图师平台最底部的沙堆</p><img src='https://i0.hdslb.com/bfs/article/37fd078b09d025a0712951bfee2acd3c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2099,
		"name": "伙伴蛋099",
		"Y": 491.869,
		"X": 623.375,
		"icon": "",
		"DESC": "<p>沙泽绘图师北部不远的一根柱子上面，非常高，可以从气球这边飘过去。</p><img src='https://i0.hdslb.com/bfs/article/b88672fbe98b7662936f0159ba80b0ef3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2100,
		"name": "伙伴蛋100",
		"Y": 477.2479,
		"X": 631.0999,
		"icon": "",
		"DESC": "<p>100号蛋稍微东北的沙堆上</p><img src='https://i0.hdslb.com/bfs/article/8bbe95e0aa846d1e0763549e4fb232793405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2101,
		"name": "伙伴蛋101",
		"Y": 417.713,
		"X": 668.9,
		"icon": "",
		"DESC": "<p>巨大红色壳状物上方</p><img src='https://i0.hdslb.com/bfs/article/b429560a4e96106f96093493997b9c6f3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2102,
		"name": "伙伴蛋102",
		"Y": 409.466,
		"X": 666.775,
		"icon": "",
		"DESC": "<p>巨大骨骼上面</p><img src='https://i0.hdslb.com/bfs/article/e5d1f98f2d3e7bf57d178fb5f883d5df3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2103,
		"name": "伙伴蛋103",
		"Y": 581.1135,
		"X": 449.75,
		"icon": "",
		"DESC": "<p>飞船北侧的管子里</p><img src='https://i0.hdslb.com/bfs/article/2a9169b01bc108917f0c1a96c18b93bf3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2104,
		"name": "伙伴蛋104",
		"Y": 588.6115,
		"X": 455.25,
		"icon": "",
		"DESC": "<p>布轮斯威克中心号尾部喷口里面</p><img src='https://i0.hdslb.com/bfs/article/7e63ed61c06f78afe768d3048a8979ef3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2105,
		"name": "伙伴蛋105",
		"Y": 599.483604,
		"X": 450.375,
		"icon": "",
		"DESC": "<p>布轮斯威克中心号船身西南侧的骨架上</p><img src='https://i0.hdslb.com/bfs/article/652fd94a39b4faa8e31b4c6fcce25b3e3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2106,
		"name": "伙伴蛋106",
		"Y": 595.7346,
		"X": 460,
		"icon": "",
		"DESC": "<p>布轮斯威克中心号船身东侧的突出部分上面</p><img src='https://i0.hdslb.com/bfs/article/6c788d275c5cf8c0e7c933675f4e8dca3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2107,
		"name": "伙伴蛋107",
		"Y": 632.959,
		"X": 428,
		"icon": "",
		"DESC": "<p>布轮斯威克中心号西南有段距离的沙丘上面的骨架上</p><img src='https://i0.hdslb.com/bfs/article/f601be4d850d4e27b6eaeeacc4ba01a33405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2108,
		"name": "伙伴蛋108",
		"Y": 585.0115,
		"X": 521.399,
		"icon": "",
		"DESC": "<p>沙虫旁边的红色岩石上</p><img src='https://i0.hdslb.com/bfs/article/648ed5b1b858b725cce83adf51f157353405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2109,
		"name": "伙伴蛋109",
		"Y": 582.512,
		"X": 518.52,
		"icon": "",
		"DESC": "<p>沙虫最上面</p><img src='https://i0.hdslb.com/bfs/article/7b5ab91839ac2512b2f545ccb8fe5b0a3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2110,
		"name": "伙伴蛋110",
		"Y": 581.637477,
		"X": 514.65,
		"icon": "",
		"DESC": "<p>沙虫嘴巴里</p><img src='https://i0.hdslb.com/bfs/article/0105b1928ec08a6a53cec32049c00dfb3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2111,
		"name": "伙伴蛋111",
		"Y": 583.137,
		"X": 562.8999,
		"icon": "",
		"DESC": "<p>沙虫出口附近，要从头到尾都走完，最后从尾部钻出来才能拿到。</p><img src='https://i0.hdslb.com/bfs/article/c61eff2ca3bf22818ae4692770568e333405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2112,
		"name": "伙伴蛋112",
		"Y": 596.496687,
		"X": 626,
		"icon": "",
		"DESC": "<p>天时台门口雕像脚旁边的屋顶</p><img src='https://i0.hdslb.com/bfs/article/ba312843723d4717f05ec4cbe0c734833405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2113,
		"name": "伙伴蛋113",
		"Y": 603.244,
		"X": 629.375,
		"icon": "",
		"DESC": "<p>天时台走廊上面</p><img src='https://i0.hdslb.com/bfs/article/e2de1d97b8f26ba5c150eb69bf85acf23405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2114,
		"name": "伙伴蛋114",
		"Y": 619.74,
		"X": 634.25,
		"icon": "",
		"DESC": "<p>天时台最里面的雕像旁边</p><img src='https://i0.hdslb.com/bfs/article/618f6b2597885958c18b293e6beea60f3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2115,
		"name": "伙伴蛋115",
		"Y": 554.49,
		"X": 653.625,
		"icon": "",
		"DESC": "<p>圆环塔顶部</p><img src='https://i0.hdslb.com/bfs/article/51f8d0262f8ca24727b36ab05ff894d93405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2116,
		"name": "伙伴蛋116",
		"Y": 167.774,
		"X": 651.025,
		"icon": "",
		"DESC": "<p>圆环塔顶部</p><img src='https://i0.hdslb.com/bfs/article/45e8d7ffe1bec5eecc29216bbc1aa0343405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2117,
		"name": "伙伴蛋117",
		"Y": 124.586,
		"X": 652.425,
		"icon": "",
		"DESC": "<p>间歇泉入口的建筑上面</p><img src='https://i0.hdslb.com/bfs/article/3dcf01ef2400d8388b43fa737899184a3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2118,
		"name": "伙伴蛋118",
		"Y": 119.962,
		"X": 653.175,
		"icon": "",
		"DESC": "<p>猎户泽比后面的洞里面</p><img src='https://i0.hdslb.com/bfs/article/0de9fca0d5b9d88693e7b176fa18dd793405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2119,
		"name": "伙伴蛋119",
		"Y": 117.963,
		"X": 649.424,
		"icon": "",
		"DESC": "<p>从有莲叶的水池旁边进去，最里面可以看到</p><img src='https://i0.hdslb.com/bfs/article/f50591c847a4067b71f4cd48f5437e0f3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2120,
		"name": "伙伴蛋120",
		"Y": 112.964571,
		"X": 651.05,
		"icon": "",
		"DESC": "<p>在柱子的中部，需要从别的柱子飘过去或利用喷泉过去。</p><img src='https://i0.hdslb.com/bfs/article/b87b670cf15328c0bccfad9e6b5ab1853405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2121,
		"name": "伙伴蛋121",
		"Y": 35.24344,
		"X": 621.375,
		"icon": "",
		"DESC": "<p>岩滩荒原北部的巨石阵</p><img src='https://i0.hdslb.com/bfs/article/9451d90b1c6bc9944149af29d1538f8b3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2122,
		"name": "伙伴蛋122",
		"Y": 75.582,
		"X": 557.225,
		"icon": "",
		"DESC": "<p>绘图员塔底部</p><img src='https://i0.hdslb.com/bfs/article/0aa1f0c9414d84414e7c0a769db09cf13405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2123,
		"name": "伙伴蛋123",
		"Y": 77.207,
		"X": 566.475,
		"icon": "",
		"DESC": "<p>绘图员塔东边的残骸内</p><img src='https://i0.hdslb.com/bfs/article/943744d422e64814e28b39617a59b7883405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2124,
		"name": "伙伴蛋124",
		"Y": 81.08,
		"X": 553.225,
		"icon": "",
		"DESC": "<p>绘图员塔西南侧残骸内，需要从塔上飘过去，从地面上去不了</p><img src='https://i0.hdslb.com/bfs/article/e017ae542ab4c0b9c9c49c8959c059cf3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2125,
		"name": "伙伴蛋125",
		"Y": 84.705,
		"X": 553.225,
		"icon": "",
		"DESC": "<p>绘图员塔西南侧残骸内，需要从塔上飘过去</p><img src='https://i0.hdslb.com/bfs/article/2d651303d6eb0a0bf9822221f27d59983405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2126,
		"name": "伙伴蛋126",
		"Y": 48.964,
		"X": 508.6,
		"icon": "",
		"DESC": "<p>岩滩荒原西北的残骸内</p><img src='https://i0.hdslb.com/bfs/article/817dcccf6c4cade1fc684b8fbdc820413405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2127,
		"name": "伙伴蛋127",
		"Y": 25.846,
		"X": 492.1,
		"icon": "",
		"DESC": "<p>崔力克之柱号尾部喷管附近的管子里</p><img src='https://i0.hdslb.com/bfs/article/990a3f409a448f79f5894634b5a4b3c43405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2128,
		"name": "伙伴蛋128",
		"Y": 24.846,
		"X": 514.6,
		"icon": "",
		"DESC": "<p>崔力克之柱号头部附近的残骸里</p><img src='https://i0.hdslb.com/bfs/article/a22ca29a85803cd4c8e0e5010a7a56fb3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2129,
		"name": "伙伴蛋129",
		"Y": 25.221,
		"X": 502.6,
		"icon": "",
		"DESC": "<p>崔力克之柱号内部</p><img src='https://i0.hdslb.com/bfs/article/66d5202ca949f80dc97c834688c034d63405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2130,
		"name": "伙伴蛋130",
		"Y": 417.099,
		"X": 129.4,
		"icon": "",
		"DESC": "<p>七姐妹站南侧有7个漂浮的石柱，要爬到最高的那个上面去。（这应该是最难拿的一个）。拿到了别马上走，从这里能飘到132号蛋那里去。</p><img src='https://i0.hdslb.com/bfs/article/f29960e172413b6531b130acc87dd7dd3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2131,
		"name": "伙伴蛋131",
		"Y": 406.4776,
		"X": 145.775,
		"icon": "",
		"DESC": "<p>七姐妹站北侧很高的石柱上，满体力也不够爬上去，要从131号蛋的那个地方飘过去</p><img src='https://i0.hdslb.com/bfs/article/15fdddbccc2344d976018a4513d11aa63405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2132,
		"name": "伙伴蛋132",
		"Y": 413.225,
		"X": 143.525,
		"icon": "",
		"DESC": "<p>七姐妹站里</p><img src='https://i0.hdslb.com/bfs/article/c97eb4d2fdac0fdee13de91873dac9463405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2133,
		"name": "伙伴蛋133",
		"Y": 505.726042,
		"X": 190.825,
		"icon": "",
		"DESC": "<p>绘图师塔最外圈的屋顶</p><img src='https://i0.hdslb.com/bfs/article/36c7ef51369c78b240b6904f751b34f43405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2134,
		"name": "伙伴蛋134",
		"Y": 502.851812,
		"X": 188.575,
		"icon": "",
		"DESC": "<p>绘图师塔突出的水晶顶部</p><img src='https://i0.hdslb.com/bfs/article/485ea5b6a2b31d2128f9ed582b2830803405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2135,
		"name": "伙伴蛋135",
		"Y": 538.61,
		"X": 150.875,
		"icon": "",
		"DESC": "<p>圆环塔</p><img src='https://i0.hdslb.com/bfs/article/5065332b36a4a67694552652164faea93405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2136,
		"name": "伙伴蛋136",
		"Y": 518.5912,
		"X": 93.025,
		"icon": "",
		"DESC": "<p>尼夫暗影飞船南侧突出部分</p><img src='https://i0.hdslb.com/bfs/article/2d0340b16e7159fc9d5ecc0336f56e193405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2137,
		"name": "伙伴蛋137",
		"Y": 514.217,
		"X": 98.65,
		"icon": "",
		"DESC": "<p>尼夫暗影飞船头部下方</p><img src='https://i0.hdslb.com/bfs/article/fffe9914d2b2b39f73946f842cd09e093405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2138,
		"name": "伙伴蛋138",
		"Y": 514.092,
		"X": 85.9,
		"icon": "",
		"DESC": "<p>尼夫暗影飞船喷嘴</p><img src='https://i0.hdslb.com/bfs/article/e93ce8be487ffa3efe932c6dbbc3a71b3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2139,
		"name": "伙伴蛋139",
		"Y": 508.2189,
		"X": 85.775,
		"icon": "",
		"DESC": "<p>尼夫暗影飞船西北侧的管子里</p><img src='https://i0.hdslb.com/bfs/article/5e46f19009c9db964539fd519c912c6c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2140,
		"name": "伙伴蛋140",
		"Y": 506.765,
		"X": 222.525,
		"icon": "",
		"DESC": "<p>伽崇的地堡雕像头上</p><img src='https://i0.hdslb.com/bfs/article/e0c6e995105442446046181274cf59683405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2141,
		"name": "伙伴蛋141",
		"Y": 506.765,
		"X": 230.9,
		"icon": "",
		"DESC": "<p>伽崇的地堡雕像身后的房间</p><img src='https://i0.hdslb.com/bfs/article/e52e175a4015a173480ef2ae669a09e33405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2142,
		"name": "伙伴蛋142",
		"Y": 506.39,
		"X": 238.025,
		"icon": "",
		"DESC": "<p>伽崇的地堡最深处的巢穴</p><img src='https://i0.hdslb.com/bfs/article/d26b845ad05393cf060f8f7ba62cfee73405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2143,
		"name": "伙伴蛋143",
		"Y": 612.9044,
		"X": 81.25,
		"icon": "",
		"DESC": "<p>水晶高原外侧边缘</p><img src='https://i0.hdslb.com/bfs/article/d78782414375243d947ce2961fc704073405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2144,
		"name": "伙伴蛋144",
		"Y": 633.024,
		"X": 73.75,
		"icon": "",
		"DESC": "<p>水晶高原中央雕像手中的水晶上面</p><img src='https://i0.hdslb.com/bfs/article/eb0c1ad8c00ac724d2bbe76e6d4561633405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2145,
		"name": "伙伴蛋145",
		"Y": 608.64,
		"X": 644.25,
		"icon": "",
		"DESC": "<p>天时台东部外侧</p><img src='https://i0.hdslb.com/bfs/article/bfd551e915e1a7ae66c17a6392b7728d3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2146,
		"name": "伙伴蛋146",
		"Y": 292.737352,
		"X": 353.875,
		"icon": "",
		"DESC": "<p>女王巢穴附近的石柱上方，体力要求高，后期来</p><img src='https://i0.hdslb.com/bfs/article/906c855b5d5698c030974fd2c53111b63405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2147,
		"name": "伙伴蛋147",
		"Y": 123.985,
		"X": 655.025,
		"icon": "",
		"DESC": "<p>间歇泉入口附近的柱子中部</p><img src='https://i0.hdslb.com/bfs/article/1e9c318adbf556c050784ff377939b833405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2148,
		"name": "伙伴蛋148",
		"Y": 106.8694,
		"X": 654.35,
		"icon": "",
		"DESC": "<p>间歇泉北侧柱子顶部</p><img src='https://i0.hdslb.com/bfs/article/52abc042992539fb1daac4c59fd969043405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2149,
		"name": "伙伴蛋149",
		"Y": 307.326,
		"X": 162.175,
		"icon": "",
		"DESC": "<p>巨鲸东侧稍微有点距离的石头上</p><img src='https://i0.hdslb.com/bfs/article/c286ff0566e09915dff278446b8d6ebd3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2150,
		"name": "伙伴蛋150",
		"Y": 301.0781,
		"X": 143.175,
		"icon": "",
		"DESC": "<p>巨鲸东侧的管子里</p><img src='https://i0.hdslb.com/bfs/article/74b82134370c4d3b23fb947ff133d3003405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2151,
		"name": "伙伴蛋151",
		"Y": 279.708918,
		"X": 117.424999,
		"icon": "",
		"DESC": "<p>巨鲸北侧的管子里</p><img src='https://i0.hdslb.com/bfs/article/4a934d003c9d3aa228390a584096ea9c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2152,
		"name": "伙伴蛋152",
		"Y": 318.573,
		"X": 131.3,
		"icon": "",
		"DESC": "<p>从巨鲸东南侧喷管进入船体，在喷管附近。</p><img src='https://i0.hdslb.com/bfs/article/8b62ce30e57b79549041b8ff7c5a13f13405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2153,
		"name": "伙伴蛋153",
		"Y": 326.82,
		"X": 128.55,
		"icon": "",
		"DESC": "<p>进入巨鲸船体后会发现红色的电缆，到电缆的另一头打开开关，这时会开启电缆对面的门，然后骑车迅速进门，才能拿到。走路来不及。</p><img src='https://i0.hdslb.com/bfs/article/a6e50f7bef1fd997b7fbf4998fa3000a3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2154,
		"name": "伙伴蛋154",
		"Y": 304.952,
		"X": 120.675,
		"icon": "",
		"DESC": "<p>在大厅红色电缆开关附近，停有三艘飞船，中间那个船头对着一个管子，里面有一只。</p><img src='https://i0.hdslb.com/bfs/article/ab22e0bd8be2274f3696ddbba71dac3d3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2155,
		"name": "伙伴蛋155",
		"Y": 309.076,
		"X": 117.05,
		"icon": "",
		"DESC": "<p>从取得155号的蛋的旁边的梯子上去，进入绿灯的门，到控制室里角落会发现一只。</p><img src='https://i0.hdslb.com/bfs/article/c8790f4d12e2e1148f210652589539ca3405001.png@1e_1c.webp'><img src='https://i0.hdslb.com/bfs/article/378e69eef3e4563fbc20865ef13820b63405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2156,
		"name": "伙伴蛋156",
		"Y": 301.203,
		"X": 134.05,
		"icon": "",
		"DESC": "<p>在巨鲸内部大厅，使用移动的黄色箱子可以来到三艘飞船上方的平台，然后通过两个长梯子到达很上面的地方，可以飘到一个管子上得到一只。</p><img src='https://i0.hdslb.com/bfs/article/c84878ba165b68acba81145b957b9e5f3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2157,
		"name": "伙伴蛋157",
		"Y": 311.07552,
		"X": 122.55,
		"icon": "",
		"DESC": "<p>返回之前来过的高层平台，从一个有缺口的地方，往下滑行到下方平台。</p><img src='https://i0.hdslb.com/bfs/article/c6277aa84e46b5972ff6cea88721f5ed3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2158,
		"name": "伙伴蛋158",
		"Y": 306.451758,
		"X": 110.1749,
		"icon": "",
		"DESC": "<p>从取得158号的地方前往途中所示的门，进去后是一个植物区域，在一层角落有一只。</p><img src='https://i0.hdslb.com/bfs/article/29979f1ebf191b898753b67943b379ab3405001.png@1e_1c.webp'><img src='https://i0.hdslb.com/bfs/article/2e082345d67e2e97ed085cc4e95004ce3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2159,
		"name": "伙伴蛋159",
		"Y": 307.076591,
		"X": 109.425,
		"icon": "",
		"DESC": "<p>和158号同样的植物区域，在第3层</p><img src='https://i0.hdslb.com/bfs/article/ba94c89fdae2165030a310e6dda505ff3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2160,
		"name": "伙伴蛋160",
		"Y": 316.824,
		"X": 123.925,
		"icon": "",
		"DESC": "<p>回到之前爬两次长梯到达的高层平台，进行远距离滑行，到一个喷管口落下可以发现一只。</p><img src='https://i0.hdslb.com/bfs/article/d79a24cbf043ac33ef6d5170113af12b3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2161,
		"name": "伙伴蛋161",
		"Y": 313.9497,
		"X": 129.674999,
		"icon": "",
		"DESC": "<p>从取得161号的管口进入，会看到一个很长的斜坡走廊，往上走，到图示位置。</p><img src='https://i0.hdslb.com/bfs/article/7375d64d2d572a371e4a5c6937f20dfe3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2162,
		"name": "伙伴蛋162",
		"Y": 323.447,
		"X": 117.55,
		"icon": "",
		"DESC": "<p>取得161号的管子，进去后往右边走，再跳下有一只。</p><img src='https://i0.hdslb.com/bfs/article/3ce148789bcadd6fdfe7471faf905b933405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2163,
		"name": "伙伴蛋163",
		"Y": 304.57726,
		"X": 134.425,
		"icon": "",
		"DESC": "<p>巨鲸大厅，红色电缆开关附近有个洞可以钻进去，下到最下面可以拿到</p><img src='https://i0.hdslb.com/bfs/article/d201eda4739b3ef6f998577cbae1a61c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "chum",
		"id": 2164,
		"name": "伙伴蛋164",
		"Y": 311.325,
		"X": 128.3,
		"icon": "",
		"DESC": "<p>巨鲸大厅，红色电缆穿过的飞船残骸，从另一边可以钻进去</p><img src='https://i0.hdslb.com/bfs/article/381fdfb0d12f474d5eaea21527215fd63405001.png@1e_1c.webp'>"
	},
	{
		"category": "quest",
		"id": 1001,
		"name": "原子校准仪（从前的船）",
		"Y": 277.875,
		"X": 558.5,
		"icon": "",
		"DESC": "<p>从初始村子里的这个山洞进去拿到，门口有个小女孩。（NPC说需要用甲虫来交换，但可以无视，直接跑去拿也行）</p><img src='https://i0.hdslb.com/bfs/article/1dc9ff9f0a1c51399af4ab9ab2096a643405001.png@1e_1c.webp'>"
	},
	{
		"category": "quest",
		"id": 1002,
		"name": "原子功能单元（从前的船）",
		"Y": 337.125,
		"X": 568.625,
		"icon": "",
		"DESC": "<p>在初始地图南部，高处的废船，需要从下面逐步爬上去才能到达。</p><img src='https://i0.hdslb.com/bfs/article/81e5f5ecc83eee924bd78189569281c83405001.png@1e_1c.webp'>"
	},
	{
		"category": "quest",
		"id": 1003,
		"name": "原子控制面板（从前的船）",
		"Y": 282,
		"X": 598.625,
		"icon": "",
		"DESC": "<p>初始地图东侧，从一个铁门处进入，先爬上铁门，使用开关打开，再进入看到飞船，接通电源后进入飞船取得道具。</p><img src='https://i0.hdslb.com/bfs/article/81e5f5ecc83eee924bd78189569281c83405001.png@1e_1c.webp'>"
	},
	{
		"category": "quest",
		"id": 1004,
		"name": "磁石激活",
		"Y": 327.875,
		"X": 591.375,
		"icon": "",
		"DESC": "<p>初始地图东南，进入神庙触发激活剧情</p>"
	},
	{
		"category": "quest",
		"id": 1005,
		"name": "勒霍尔山洞",
		"Y": 57.554,
		"X": 580.5,
		"icon": "",
		"DESC": "<p>娱人面具相关任务：寻找勒霍尔。要前往山洞，需要从地图绘制师那里飘过去到一个平台，再攀几个柱子就能到达。</p><img src='https://i0.hdslb.com/bfs/article/ca3030d3699b2ae50b59e3f6115e3e5c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "quest",
		"id": 1006,
		"name": "五铃（攀登面具相关地点）",
		"Y": 132.75,
		"X": 72.5,
		"icon": "",
		"DESC": "<p>艾克利亚西边，可以接到和井相关的获取攀爬表章的任务</p><img src='https://i0.hdslb.com/bfs/article/dfaed482faac796644857b8ccf5c41373405001.png@1e_1c.webp'>"
	},
	{
		"category": "balloon",
		"id": 3000,
		"name": "地图绘制员（沙泽）",
		"Y": 504.995,
		"X": 623.9,
		"icon": "",
		"DESC": ""
	},
	{
		"category": "balloon",
		"id": 3001,
		"name": "地图绘制员（赤海）",
		"Y": 164.7967,
		"X": 220.65,
		"icon": "",
		"DESC": ""
	},
	{
		"category": "balloon",
		"id": 3002,
		"name": "地图绘制员（沙海）",
		"Y": 257.549238,
		"X": 456.95,
		"icon": "",
		"DESC": ""
	},
	{
		"category": "balloon",
		"id": 3003,
		"name": "地图绘制员（岩滩荒原）",
		"Y": 78.735423,
		"X": 558.375,
		"icon": "",
		"DESC": ""
	},
	{
		"category": "balloon",
		"id": 3004,
		"name": "地图绘制员（恶地）",
		"Y": 402.66,
		"X": 314.75,
		"icon": "",
		"DESC": ""
	},
	{
		"category": "balloon",
		"id": 3005,
		"name": "地图绘制员（执壶峡谷）",
		"Y": 284.865,
		"X": 564.25,
		"icon": "",
		"DESC": ""
	},
	{
		"category": "balloon",
		"id": 3006,
		"name": "地图绘制员（哈克阿）",
		"Y": 501.767,
		"X": 188.3,
		"icon": "",
		"DESC": ""
	},
	{
		"category": "station",
		"id": 4000,
		"name": "伊别克斯营地",
		"Y": 287.224838,
		"X": 557.75,
		"icon": "settlement",
		"DESC": "<p>初始村子</p>"
	},
	{
		"category": "temple",
		"id": 4001,
		"name": "风塔",
		"Y": 379.7113,
		"X": 580.5,
		"icon": "",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/0e14c1325893014fecd4c97da4f6bdd73405001.png@1e_1c.webp'>"
	},
	{
		"category": "station",
		"id": 4002,
		"name": "钓手小屋（沙海）",
		"Y": 321.2727,
		"X": 432.55,
		"icon": "settlement",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/15b92b7c0a3159f9d71b7af69a4215833405001.png@1e_1c.webp'><p>和NPC对话进行三次钓鱼任务，获取钓手面具</p>"
	},
	{
		"category": "temple",
		"id": 4003,
		"name": "赫拉克勒斯甲虫窝",
		"Y": 403.7256,
		"X": 439.575,
		"icon": "",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/dec6affdd5bf3993f12e193e60a6db953405001.png@1e_1c.webp'>"
	},
	{
		"category": "station",
		"id": 4004,
		"name": "焦橡站",
		"Y": 205.418,
		"X": 416.274998,
		"icon": "camp",
		"DESC": "<p>沙海聚落</p><img src='https://i0.hdslb.com/bfs/article/3c2fc192c4ba393badd5b8797c1513ac3405001.png@1e_1c.webp'>"
	},
	{
		"category": "station",
		"id": 4005,
		"name": "伙伴窝（女王）",
		"Y": 298.75,
		"X": 353.125,
		"icon": "queen",
		"DESC": "<p>交换伙伴蛋的地方，100颗可以升级满体力，获得伙伴面具，并完成奖杯，如果收集完全部165颗，能够获得伙伴衣服和裤子。</p><img src='https://i0.hdslb.com/bfs/article/45809049f643661208b11ad15a48c3d93405001.png@1e_1c.webp'>"
	},
	{
		"category": "station",
		"id": 4006,
		"name": "艾克利亚",
		"Y": 157.444,
		"X": 119.525,
		"icon": "",
		"DESC": "<p>赤海村落</p><img src='https://i0.hdslb.com/bfs/article/d5fd2c2de9350f0fe8a921549788fd023405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4007,
		"name": "贼喜鹊阶梯井",
		"Y": 183.884934,
		"X": 90.425,
		"icon": "",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/2c74094adfe0da752627982c75d6356c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4008,
		"name": "原子之心",
		"Y": 57.351511,
		"X": 105.6,
		"icon": "",
		"DESC": "<p>要接到城中心碎任务才能进去</p><img src='https://i0.hdslb.com/bfs/article/11e50c91f3d071fdf977215458a1e17a3405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4009,
		"name": "叛桥",
		"Y": 363.9055,
		"X": 222.35,
		"icon": "",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/3492a6eb8e381729b858498389f350753405001.png@1e_1c.webp'>"
	},
	{
		"category": "station",
		"id": 4010,
		"name": "七姐妹站",
		"Y": 409.2185,
		"X": 140.25,
		"icon": "camp",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/c910f195e8afce30e7a630b4bebdd9e13405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4011,
		"name": "伽崇的地堡",
		"Y": 504.22,
		"X": 230.5,
		"icon": "cave",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/62bb41e30274369646f79065f3422d173405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4012,
		"name": "冷光虫洞穴",
		"Y": 622.23,
		"X": 263.75,
		"icon": "cave",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/d0655ee3d402a9d3527b40d0909d218a3405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4013,
		"name": "神秘的神殿",
		"Y": 429.58,
		"X": 396.35,
		"icon": "",
		"DESC": "<p>在世界各地收集6个圆环后，安放在这里，最后和NPC海帕对话，完成奖杯“速度与好奇”</p><img src='https://i0.hdslb.com/bfs/article/5e1f239135711fe6de122ec4400a7f013405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4014,
		"name": "生灵屋",
		"Y": 551.25,
		"X": 454.55,
		"icon": "",
		"DESC": "<p>可以在这里提交鱼类和蝴蝶、蜻蜓，完成奖杯“生灵满屋”。</p><img src='https://i0.hdslb.com/bfs/article/b2ad073d1ed056d383349df0bc2118b03405001.png@1e_1c.webp'>"
	},
	{
		"category": "station",
		"id": 4015,
		"name": "髓骨站",
		"Y": 525.756858,
		"X": 551.9,
		"icon": "camp",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/2185acd7e3bced262bb8d3513de5288c3405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4016,
		"name": "沙虫",
		"Y": 571.164,
		"X": 531.6999,
		"icon": "sandwyrm",
		"DESC": "<p>沙虫嘴巴可以爬进去，里面可以获得沙虫面具，以及从尾巴出来可以拿到112号蛋</p><img src='https://i0.hdslb.com/bfs/article/23f74b26fbc4db286ae8e1e65ea7a7ee3405001.png@1e_1c.webp'>"
	},
	{
		"category": "station",
		"id": 4017,
		"name": "间歇泉石柱",
		"Y": 122.123,
		"X": 648.875,
		"icon": "cave",
		"DESC": "<img src='https://i0.hdslb.com/bfs/article/99e5db68d8f7d4bb04f27f621f0e429e3405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4018,
		"name": "崔力克之柱号",
		"Y": 25.29488,
		"X": 508.575,
		"icon": "ship",
		"DESC": ""
	},
	{
		"category": "temple",
		"id": 4019,
		"name": "邓博因号",
		"Y": 189.5,
		"X": 475.875,
		"icon": "ship",
		"DESC": ""
	},
	{
		"category": "temple",
		"id": 4020,
		"name": "拜尔弗隆联道号",
		"Y": 235,
		"X": 242.5,
		"icon": "ship",
		"DESC": ""
	},
	{
		"category": "temple",
		"id": 4021,
		"name": "尼夫暗影号",
		"Y": 514.2919,
		"X": 92.75,
		"icon": "ship",
		"DESC": ""
	},
	{
		"category": "temple",
		"id": 4022,
		"name": "劳利斯路号",
		"Y": 525.24738,
		"X": 376.59999,
		"icon": "ship",
		"DESC": ""
	},
	{
		"category": "temple",
		"id": 4023,
		"name": "肯博的立方",
		"Y": 465.1034,
		"X": 384,
		"icon": "ship",
		"DESC": "<p>这飞船没AI终端</p>"
	},
	{
		"category": "temple",
		"id": 4024,
		"name": "布伦斯威克中心号",
		"Y": 597.89,
		"X": 455.2,
		"icon": "ship",
		"DESC": ""
	},
	{
		"category": "temple",
		"id": 4025,
		"name": "巨鲸",
		"Y": 291,
		"X": 120.75,
		"icon": "whale",
		"DESC": "<p>这里可以完成“历史爱好者”奖杯，即找到最后一个AI。另外还有大量的废金属可以捡。<p>"
	},
	{
		"category": "temple",
		"id": 4026,
		"name": "天时台",
		"Y": 614.58,
		"X": 636,
		"icon": "watch",
		"DESC": "<p>解谜思路<br>1.进入面具上方的平台，按下开关打开穹顶。<br>2.另外三个方向出现三个操作台，操作每个让上面的机关变成蓝色。开启面具前的机关<br>3.面具前的机关可以取得光球，拿出光球，放入旁边每个石碑。<br>4.从日出开始等，一定可以照到某个图案，从而开启面具大门</p><img src='https://i0.hdslb.com/bfs/article/9e43f851a77dd06395b1b8e544c635aa3405001.png@1e_1c.webp'>"
	},
	{
		"category": "temple",
		"id": 4027,
		"name": "晶石高原",
		"Y": 639.21,
		"X": 74.75,
		"icon": "watch",
		"DESC": "<p>这里的闪电水晶是游戏中售给商人价格最高的道具，100一个</p><img src='https://i0.hdslb.com/bfs/article/7b7b5dabb32e73d8f591cfcbe2d3cc743405001.png@1e_1c.webp'>"
	},

	]

}