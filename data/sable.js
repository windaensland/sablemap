var SW, NE;
if (window.innerWidth <= 480) {  
    SW = L.latLng(-800, -300);
    NE = L.latLng(115, 900);
} else {
    SW = L.latLng(-800, -500);  
    NE = L.latLng(130, 1300);    
}
var bounds = L.latLngBounds(SW, NE);

var map = L.map('sableMap', {
	crs: L.CRS.Simple,
	minZoom: 0,
	maxZoom: 3,
	maxNativeZoom: 3,
	maxBounds: bounds,
	zoom: 3,
	tap: false,
	doubleClickZoom: true,
});

var tile = {
	tileSize: L.point(705, 705)
};

sableMapTiles = L.tileLayer('pics/tiles/{z}/{x}/{y}.webp', tile)

sableMapTiles.addTo(map)
map.setView([ -350, 450], 2)  
map.zoomControl.setPosition('bottomleft') 

map.on('mousemove', (i) => {
    document.getElementById("coordinate").innerHTML = "当前坐标（Y/X）：" + i.latlng.lat + ", " + i.latlng.lng;
});

var customPopup = {
	'maxWidth': '300',
	'className': 'customPopup' 
},
 contentPopup = "";

var MarkerIconSize = 50,
MarkerIconCenter = MarkerIconSize / 2 -3.5,
MarkerPopPosition = -MarkerIconSize - 5,
markerGroups = {
	temple: [],
	balloon: [],
	station: [],
	chum: [],
	quest: [],
  };

var polylinePoints = [
    L.latLng(-77.125, 560.625),
    L.latLng(-59.125, 567.625),
    L.latLng(-55.755, 580.75)
  ];
var polyline = L.polyline(polylinePoints);


for (i = 0; i < itemData.items.length; i++) {
	  let j = itemData.items[i],
	  MarkerIconPic;
	  if (j.icon === '') {
  	  MarkerIconPic = 'pics/icons/' + j.category + '.webp';
	  } else {
  	  MarkerIconPic = 'pics/icons/' + j.icon + '.webp';
	  }
    j.DESC = j.DESC.replace(/\.webp'/g, ".webp' onclick='openImage(this.src)'");

 
    if (j.category === 'chum') {
      let num = parseInt(j.name.slice(3));  
      let newNum = num + 1;  
      let newStr = newNum.toString().padStart(3, '0');  
      j.name = '伙伴蛋' + newStr;  
    }

	  icon = L.icon({
		  iconUrl: MarkerIconPic,
		  iconSize: [50, ],
		  iconAnchor: [MarkerIconCenter, MarkerIconSize],
		  shadowSize: [0, 0],
		  popupAnchor: [3, MarkerPopPosition], 
	   });
  
     let marker = new L.marker(new L.latLng([-j.Y, j.X]), {
      icon: icon,
      title: j.name
      }).bindPopup("<h3>" + j.name + "</h3><br>"+ j.DESC, customPopup
      ).on('click', (i)=> {  
      let latlng = i.target.getLatLng();
          latlng.lat += 35;
          map.setView(latlng, map.getZoom());
      if (j.id === 1005) {
          polyline.addTo(map);
      } else {
          if (map.hasLayer(polyline)) {
              map.removeLayer(polyline);
          }
      }
  });

	  if (markerGroups.hasOwnProperty(j.category)) {
    	markerGroups[j.category].push(marker);
		} else {
    	console.error('Category ' + j.category + '类型不存在');
		}
   }
  
  
  var layers = {};
  for (var i in markerGroups) {
	layers[i] = L.layerGroup(markerGroups[i]);
  }
  for (var i in layers) {
	map.addLayer(layers[i]);
  }

function toggleMarkers(category, index) {
    let layer = layers[category];
    let checker = document.getElementById(category + 'Checker');
    let itemsDiv = document.getElementById('item-' + index);  

    if (map.hasLayer(layer)) {
        map.removeLayer(layer);
        checker.style.display = 'none';
        itemsDiv.style.textDecoration = 'line-through';  
    } else {
        map.addLayer(layer);
        checker.style.display = 'block';
        itemsDiv.style.textDecoration = 'none';  
    }
}


var allLayers = L.layerGroup();
for (var category in layers) {
    allLayers.addLayer(layers[category]);
}

var searcher = new L.Control.Search({
    position:'topleft',		
    container:'searcher',
    textPlaceholder: '请输入物品名称',
    textErr: '未搜索到相关内容',
    layer: allLayers,  
    initial: false, 
    zoom: 4, 
    delayType:40, 
    collapsed:false, 
    marker: false})
map.addControl(searcher);

searcher.on('search:locationfound', (i) => {    
  i.layer.openPopup();
  let latlng = i.latlng;
  map.setView(latlng, map.getZoom());
  if (window.innerWidth <= 480) {
    toggleSidePanel();
  }
});   



var places =[
  {name: '沙 海', coordinates: [-240, 389.5]},
  {name: '恶 地', coordinates: [-491.5, 312]},
  {name: '赤 沙', coordinates: [-172, 153.5]},
  {name: '沙 泽', coordinates: [-597, 491]},
  {name: '岩滩荒原', coordinates: [-106, 562.5]},
  {name: '哈克阿', coordinates: [-561, 117.5]},
  {name: '执壶河谷', coordinates: [-298.5, 554.5]},
]

for (i = 0; i < places.length; i++) {
  var place = places[i];
  var areaName = L.divIcon({className: 'areaName', html: place.name});
  L.marker(place.coordinates, {icon: areaName}).addTo(map);
}